package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        String Off = null;

        //строку в список строк
        if(statement == null || statement.length() < 3 || statement.contains(",")) return null;// проверка корректности

        int q = 0;  // для подсчета четности скобок
        int qq = 0;

        String str = statement;
        char[] result1 = str.toCharArray();
        for (int i = 0; i < result1.length; i++) {
            if(result1[i] == '(') q++;
            if(result1[i] == ')') qq++;
            for (int j = i + 1; j < result1.length ; j++) { // еще првоерка корректности //**

                if((result1[i] == '/' || result1[i] == '*' || result1[i] == '+' || result1[i] == '-' || result1[i] == '.') && result1[i] == result1[j]) {

                    return null;
                }
            }
        }

        if(q != qq) return null;

        String expression = statement.replace("/"," / ").replace("*"," * ")
                .replace("+"," + ").replace("-"," - ")   // добавление пробелов
                .replace("("," ( ").replace(")"," ) ")
                .replaceAll("  "," ");


        List<String> strList = new ArrayList<String>();

        for (String listElement : expression.trim().split(" ")) {
            strList.add(listElement);
            strList.add(" ");
        }
        strList.remove(strList.size() - 1);

//   поиск "(" в списке если находим, то преобразуем все символы от '(' до последнего ')' в строку.

        if (strList.indexOf("(") != -1) {
//          Если "(" обнаружен, ищем подходящую конструкцию

            for (int i = strList.indexOf("(") + 1; i < strList.size(); i++) {
//
//             1 первым элиментом, который мы отыскали были вторые "("
                String recursion = "";
                if (strList.get(i).equals("(")) {
                    for (int j = i; j < strList.lastIndexOf(")"); j++) {
                        recursion += strList.get(j);
                    }
                    // сверху считывали последовательность находящуюся в скобках (())
                    String test = expression.substring(expression.indexOf("("), expression.lastIndexOf(")") + 1);
                    // test - последовательность как и сверху, но с добавлением скобок по краям

                    String testRecursion = String.valueOf(evaluate(recursion));
                    expression = expression.replace(test, testRecursion);
                    // преобразовали нашу строку с использование рекруси. Избавились от первых скобок
                    strList.removeAll(strList);
                    for (String newElement : expression.trim().split(" ")) {
                        strList.add(newElement);
                        strList.add(" ");
                    }

                }

//                2 первым элементом, который мы отыскали был  ")"
                String recursion2 = "";
                if (")".equals(strList.get(i))) {

                    for (int j = strList.indexOf("(") + 1; j < strList.indexOf(")"); j++) {
                        recursion2 += strList.get(j);
                    }
                    String test2 = expression.substring(expression.indexOf("("), expression.lastIndexOf(")") + 1);
                    String testRecursion2 = String.valueOf(evaluate(recursion2));
                    expression = expression.replace(test2, testRecursion2);
                    for (String newElement : expression.trim().split(" ")) {
                        strList.add(newElement);
                        strList.add(" ");
                    }
                }
            }
        }

        List<String> stringList2 = new ArrayList<String>();

        for (String element : expression.trim().split(" ")) {
            stringList2.add(element);
        }

        while (stringList2.size() != 0) {

            Double result = 0d;

            if (stringList2.indexOf("/") != -1) {
                int index = stringList2.indexOf("/");
                if (Double.valueOf(stringList2.get(index + 1)) == 0) return null;
                result = Double.valueOf(stringList2.get(index - 1)) / Double.valueOf(stringList2.get(index + 1));
                stringList2.add(index - 1, String.valueOf(result));
                stringList2.remove(index + 2);
                stringList2.remove(index + 1);
                stringList2.remove(index);
            }
            else if (stringList2.indexOf("*") != -1) {
                int index = stringList2.indexOf("*");
                result = Double.valueOf(stringList2.get(index - 1)) * Double.valueOf(stringList2.get(index + 1));
                stringList2.add(index - 1, String.valueOf(result));
                stringList2.remove(index + 2);
                stringList2.remove(index + 1);
                stringList2.remove(index);
            }
            else if (stringList2.indexOf("-") != -1) {
                int index = stringList2.indexOf("-");
                int lastIndex = stringList2.lastIndexOf("-");
                if (index == 0) {
                    result = 0.0 - Double.valueOf(stringList2.get(index + 1));
                    stringList2.add(0, String.valueOf(result));
                    stringList2.remove(2);
                    stringList2.remove(1);
                }
                else if ((lastIndex-2>0) && (stringList2.get(lastIndex-2).equals("-"))){
                    result = Double.valueOf(stringList2.get(lastIndex + 1)) + Double.valueOf(stringList2.get(lastIndex - 1));
                    stringList2.add(lastIndex - 1, String.valueOf(result));
                    stringList2.remove(lastIndex + 2);
                    stringList2.remove(lastIndex + 1);
                    stringList2.remove(lastIndex);
                }
                else {
                    result = Double.valueOf(stringList2.get(index - 1)) - Double.valueOf(stringList2.get(index + 1));
                    stringList2.add(index - 1, String.valueOf(result));
                    stringList2.remove(index + 2);
                    stringList2.remove(index + 1);
                    stringList2.remove(index);
                }
            }
            else if (stringList2.indexOf("+") != -1) {
                int index = stringList2.indexOf("+");
                result = Double.valueOf(stringList2.get(index - 1)) + Double.valueOf(stringList2.get(index + 1));
                stringList2.add(index - 1, String.valueOf(result));
                stringList2.remove(index + 2);
                stringList2.remove(index + 1);
                stringList2.remove(index);
            }

            if ((stringList2.indexOf("*") == -1) && (stringList2.indexOf("/") == -1) && (stringList2.indexOf("+") == -1) && (stringList2.indexOf("-") == -1)) {
                if (result % 1 == 0) {
                    return String.format("%.0f", result);
                }
                else  return String.valueOf(result);
            }
        }
        return "";
    }

}
