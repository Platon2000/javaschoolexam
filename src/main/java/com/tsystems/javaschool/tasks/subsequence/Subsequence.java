package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;



public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
// если елементы равны, то увеличиваем оба индекса, если нет, то только второй
        // если первым закончился первый список, то true

        if (x!=null && y!= null) {
            try {
                boolean isContain1 = x.contains(1);
                String one = null;
                String two = null; // сами элементы
                int one1 = 0;
                int two1 = 0;
                int i = 0; //индексы элементов в списках
                int j = 0;
                boolean res = false;

                if (x.isEmpty()) return res = true;
                if (y.isEmpty()) return res = false;

                while (i <= x.size() || j <= y.size()) {
                    if(isContain1 == true) {   // если работаем с цифрами
                        one1 = Integer.parseInt(String.valueOf(x.get(i)));
                        two1 = Integer.parseInt(String.valueOf(y.get(j)));
                        if (one1 == two1 ) {
                            i++;
                        }
                        j++;
                    } else {
                        one = String.valueOf(x.get(i));  // если с буквами
                        two = String.valueOf(y.get(j));
                        if (one == two ) {
                            i++;
                        }
                        j++;
                    }

                    if (i == x.size()) return res = true;
                    if (j == y.size()) return res = false;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException();
            }
        } else  throw new IllegalArgumentException();

        return false;
    }
}
