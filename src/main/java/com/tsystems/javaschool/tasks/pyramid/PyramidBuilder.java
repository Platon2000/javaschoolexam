package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers)  {
        // TODO : Implement your solution here
        try {
            boolean can;//получится или не получится построить
            int[][] matrix;

            int size = inputNumbers.size();

            int count = 0;
            int str = 1;
            int stlb = 1;

            while (count < size) { // проверка возможности постороения
                count = count + str;
                str++;
                stlb = stlb + 2;
            }
            str = str - 1;
            stlb = stlb - 2;

            if (size == count) {
                can = true;
            } else can = false;

            if (can == true) {

                List<Object> sorted = inputNumbers.stream().sorted().collect(Collectors.toList());

                matrix = new int[str][stlb];
                for (int[] row : matrix) {
                    Arrays.fill(row, 0); // зануляем
                }

                int center = (stlb >> 1);
                count = 1; // сколько чисел будет в строке
                int arrIdx = 0;

                for (int i = 0, offset = 0; i < str; i++, offset++, count++) {
                    int start = center - offset;
                    for (int j = 0; j < count << 1; j += 2, arrIdx++) {
                        matrix[i][start + j] = (int) sorted.get(arrIdx);
                    }
                }

            }
            else {
                throw new CannotBuildPyramidException();
            }
            return matrix;

        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }


}
